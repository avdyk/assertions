# Assertions

Projet d'exemple d'utilisations des assertions: le mot-clé java, les
Assertions de Spring, celles d'AssertJ.

Site généré par Maven (avec Javadoc et rapports des tests): https://avdyk.gitlab.io/assertions

Sources, tickets, etc: https://gitlab.com/avdyk/assertions

## Présentation du projet

Il s'agit simplement d'une classe qui représente une date sous forme de chaîne
de caractères. La classe est immuable et ne peut être construite qu'en passant
par une fabrique.

La classe n'est pas complètement développée. J'ai mis le nécessaire pour
permettre un pied d'entrée dans le débuggage avec les assertions.

## Spring Assert

### Utilisation

Les premières instructions de toute méthode devrait être de tester les variables
reçues en paramètre: fixer les préconditions. Si ces préconditions ne sont pas
remplies, la méthode devrait soulever une exception appropriée (souvent une
`IllegalArgumentException`).

Les méthodes de la classe `org.springframework.util.Assert` servent à cela.

Vous trouverez les utilisations dans la méthode `DatePop.createDatePop(String)`.

### Références

- https://www.baeldung.com/spring-assert
- https://github.com/spring-projects/spring-framework/blob/master/spring-core/src/main/java/org/springframework/util/Assert.java
- https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/util/Assert.html
- https://stackoverflow.com/a/2441013/1380534
- http://www.pgbovine.net/programming-with-asserts.htm

## Le mot-clé assert

### Utilisation

La première chose à avoir à l'esprit est que ce mot clé n'a d'effet que si une
application est lancée avec l'argument `-enableassertions[:package name"..." | :class name ]`
ou `-ea[:package name"..." | :class name ]`.

On ne peut donc pas compter sur ce mot clé pour tester des variables, mais plutôt
pour utiliser en développement et pour documenter du code.

L'utilisation des `assert` vous permettra notamment de ne pas devoir tester une
valeur nulle.

Les meilleures pratiques selon l'article de **Baeldung**:

1. Toujours vérifier les valeurs nulles, les `Optional` vides quand cela
vaut la peine
1. Ne pas utiliser les `assert` pour vérifier les arguments des méthodes publiques
mais plutôt les exceptions non vérifiées comme `IllegalArgumentException` ou
`NullPointerException` (par exemple en utilisant l'`Assert` de _Spring_)
1. Ne pas appeler de méthode dans les `assert` mais plutôt effectuer les assignations
dans des variables locales et utiliser ces variables dans l'`assert`
1. Les meilleurs endroits pour les `assert` sont les endroits où le code n'est
pas censé être exécuté comme dans un `case default` ou après une boucle qui ne
devrait jamais se terminer

Une bonne pratique à ajouter est de placer les `assert` pour documenter ce que
l'on attend des arguments d'une méthode **privée**! Toutes les méthodes de la
classe devront impérativement respecter les règles.

### Références

- https://www.baeldung.com/java-assert
- http://www.pgbovine.net/programming-with-asserts.htm
- https://docs.oracle.com/javase/7/docs/technotes/guides/language/assert.html

## AssertJ

### Utilisation

Dans les classes de test.

### Références

- https://assertj.github.io/doc/
