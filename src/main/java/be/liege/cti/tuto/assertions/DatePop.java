package be.liege.cti.tuto.assertions;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

/**
 * Exemple d'utilisation des assertions.
 * Dans cette classe, utilisation du mot-clé java <code>assert</code> {@link #DatePop(String)};
 * ainsi que de la classe {@link Assert} de Spring dans la méthode {@link #createDatePop(String)}.
 */
public final class DatePop {

    private static final Pattern QUE_DES_NOMBRES = Pattern.compile("\\d+");
    private final String datePop;

    /**
     * Construction d'une date.
     * La méthode est privée, il faut passer par la fabrique {@link #createDatePop(String)} pour
     * construire la date. Donc l'argument doit répondre aux exigences internes de la classe.
     * L'argument a dû être vérifié par toutes les méthodes appelantes de la classe.
     * À des fin de documentation, on utilisera le mot clé <code>assert</code> pour documenter
     * les prérequis.
     * Il faudra exécuter le programme avec l'argument <code>-ea</code> pour que les assertions
     * soient exécutées.
     * Il est recommandé de n'utiliser le mot clé <code>assert</code> que dans les méthodes privées
     * car il ne s'agit pas d'une vérification.
     *
     * @param d la date
     */
    private DatePop(final String d) {
        assert d != null : "La date ne peut être nulle";
        assert d.length() == 8 || d.length() == 10 : "La date est une chaîne de 8 ou 10 caractères";
        assert QUE_DES_NOMBRES.matcher(d).matches()
                : "La date doit contenir uniquement des chiffres!";
        // TODO plein de validations
        datePop = d;
    }

    /**
     * Fabrique de date.
     *
     * @param date une date
     * @return une date
     * @throws IllegalArgumentException si la date est nulle, vide, ou que la taille
     *                                  de la chaîne n'est pas de 8 ou 10 caractères.
     */
    public static DatePop createDatePop(final String date) {
        Assert.notNull(date, "La date ne peut être nulle!");
        Assert.isTrue(!date.trim().isBlank(), "La chaîne ne peut être vide");
        Assert.isTrue(date.length() == 8 || date.length() == 10,
                "La date doit contenir 8 ou 10 caractères");
        Assert.isTrue(QUE_DES_NOMBRES.matcher(date).matches(),
                "La date doit contenir uniquement des chiffres!");
        return new DatePop(date);
    }

    @Override
    public String toString() {
        return datePop;
    }
}
