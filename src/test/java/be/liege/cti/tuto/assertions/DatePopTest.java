package be.liege.cti.tuto.assertions;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class DatePopTest {

    @Test
    void pas_de_date_nulle() {
        Assertions.assertThatThrownBy(() -> DatePop.createDatePop(null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("La date ne peut être nulle!");
    }

    @Test
    void pas_de_date_vide() {
        Assertions.assertThatThrownBy(() -> DatePop.createDatePop("    "))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("La chaîne ne peut être vide");
    }

    @Test
    void pas_de_longueur_inf_8() {
        Assertions.assertThatThrownBy(() -> DatePop.createDatePop("1234567"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("La date doit contenir 8 ou 10 caractères");
    }

    @Test
    void pas_de_longueur_eg_9() {
        Assertions.assertThatThrownBy(() -> DatePop.createDatePop("123456789"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("La date doit contenir 8 ou 10 caractères");
    }

    @Test
    void pas_de_longueur_sup_10() {
        Assertions.assertThatThrownBy(() -> DatePop.createDatePop("12345678901"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("La date doit contenir 8 ou 10 caractères");
    }

    @Test
    void date_de_8_char() {
        Assertions.assertThat(DatePop.createDatePop("12345678").toString())
                .isEqualTo("12345678");
    }

    @Test
    void date_de_10_char() {
        Assertions.assertThat(DatePop.createDatePop("1234567890").toString())
                .isEqualTo("1234567890");
    }

    @Test
    void ne_contient_pas_de_lettre() {
        Assertions.assertThatThrownBy(() -> DatePop.createDatePop("12ea5678"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("La date doit contenir uniquement des chiffres!");
    }
}
